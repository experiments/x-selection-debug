#include <stdio.h>
#include <X11/Xlib.h>

int main(int argc, char *argv[])
{
	Display *dpy;
	Window owner;
	Atom sel;

	if (argc < 2) {
		fprintf(stderr, "usage: xowner <selection>\n");
		return 1;
	}

	dpy = XOpenDisplay(NULL);
	if (!dpy) {
		fprintf(stderr, "Could not open X display\n");
		return 1;
	}

	sel = XInternAtom(dpy, argv[1], False);
	owner = XGetSelectionOwner(dpy, sel);
	printf("0x%lX\n", owner);

	XCloseDisplay(dpy);
	return 0;
}
