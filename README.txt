Some simple tools to analyze X selection buffers to understand what is going
on when text is selected with the mouse in X11.


BACK STORY
==========

When vim is compiled with the +clipboad and +xterm_clipboard functionalities,
and setmouse=a is set, using vim in combination with xterm allows this
workflow:

 1. Select something with the mouse;
 2. exit vim;
 3. paste the previously selected text with the middle mouse click.

Moving from xterm to gnome-terminal this workflow is not possible by default.

It turns out this is not a missing feature of vte or gtk, but rather some
extra functionality offered only by the combination of vim and xterm.

Vim copies to the CUT_BUFFER0 selection when it exits and xterm is able to
paste from CUT_BUFFER0 when it receives a middle click.

To make the workflow from above possible again it is possible to have vim
preserve the PRIMARY selection when it exits with a command like the following
in ~/.vimrc:

  autocmd VimLeave * call system("xsel -ip", getreg('*'))

All the details of how the issue have been debugged can be found at
https://bugzilla.gnome.org/show_bug.cgi?id=783828


REFERENCES
==========

https://www.jwz.org/doc/x-cut-and-paste.html
https://www.uninformativ.de/blog/postings/2017-04-02/0/POSTING-en.html
https://stackoverflow.com/questions/6453595/prevent-vim-from-clearing-the-clipboard-on-exit
https://vi.stackexchange.com/questions/3769/can-vim-add-to-the-x11-clipboard-or-primary-buffers-without-an-external-utility#comment5354_3769
https://groups.google.com/forum/#!topic/vim_dev/nlyA7AN9mfQ
https://groups.google.com/forum/#!topic/vim_use/NtprNch2ZkQ
