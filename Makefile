CFLAGS ?= -std=c99 -pedantic -pedantic-errors -Wall -g3 -O2 -D_ANSI_SOURCE_
CFLAGS += -fno-common \
	  -Wall \
	  -Wdeclaration-after-statement \
	  -Wextra \
	  -Wformat=2 \
	  -Winit-self \
	  -Winline \
	  -Wpacked \
	  -Wp,-D_FORTIFY_SOURCE=2 \
	  -Wpointer-arith \
	  -Wlarger-than-65500 \
	  -Wmissing-declarations \
	  -Wmissing-format-attribute \
	  -Wmissing-noreturn \
	  -Wmissing-prototypes \
	  -Wnested-externs \
	  -Wold-style-definition \
	  -Wredundant-decls \
	  -Wsign-compare \
	  -Wstrict-aliasing=2 \
	  -Wstrict-prototypes \
	  -Wundef \
	  -Wunreachable-code \
	  -Wunused-variable \
	  -Wwrite-strings

ifneq ($(CC),clang)
  CFLAGS += -Wunsafe-loop-optimizations
endif

EXECUTABLE := xowner
LDLIBS := -lX11

$(EXECUTABLE):

clean:
	rm -f *~ *.o $(EXECUTABLE)

test: $(EXECUTABLE)
	valgrind --leak-check=full --show-reachable=yes ./$(EXECUTABLE) PRIMARY
