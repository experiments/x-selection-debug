#!/bin/sh

[ -x ./xowner ] || make

get_owner() {
  echo -n "$1"
  OWNER_ID=$(./xowner $1)
  if [ $OWNER_ID != "0x0" ];
  then
    OWNER="$OWNER_ID"
    xwininfo -id $OWNER_ID > /dev/null 2>&1;
    if [ $? -eq 0 ];
    then
      OWNER_NAME=$(xprop -id $OWNER_ID | grep "^WM_NAME" | cut -d ' ' -f 3-)
      if [ -n "$OWNER_NAME" ];
      then
        OWNER="$OWNER $OWNER_NAME"
      else
        PARENT_ID=$(xwininfo -children -id $OWNER_ID | sed -n -e '/Parent window id:/s/.*window id: \([^ ]*\) .*$/\1/p')
        if [ -n "$PARENT_ID" ];
        then
          OWNER="$OWNER children of $PARENT_ID"

          PARENT_NAME=$(xprop -id $PARENT_ID | grep "^WM_NAME" | cut -d ' ' -f 3-)
          if [ -n "$PARENT_NAME" ];
          then
            OWNER="$OWNER $PARENT_NAME"
          fi
        fi
      fi
      echo -n " owned by $OWNER"
    fi
  fi

  echo ":"
}


get_owner "PRIMARY"
xclip -out -selection primary 2> /dev/null
echo
echo

get_owner "SECONDARY"
xclip -out -selection secondary 2> /dev/null
echo
echo

get_owner "CLIPBOARD"
xclip -out -selection clipboard 2> /dev/null
echo
echo

get_owner "CUT_BUFFER0"
xcb -p 0
echo
echo
